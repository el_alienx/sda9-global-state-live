import Card from "./Card";
import PictureDog from "../images/dog.jpg";
import PictureCat from "../images/cat.jpg";
import PictureBird from "../images/cat-food.jpg";

export default function PageA() {
  return (
    <div className="content">
      <h1>Pets ready for adoption</h1>

      <div className="grid">
        <Card title="Puppy" image={PictureDog} />
        <Card title="Whiskers" image={PictureCat} />
        <Card title="Birdie" image={PictureBird} />
      </div>
    </div>
  );
}
