// NPM Package
import { useRecoilState } from "recoil";

// Project files
import { petState } from "../state/userData";
import { clickState } from "../state/siteAnalytics";

export default function Card({ title, image }) {
  // State
  const [pet, setPet] = useRecoilState(petState);
  const [click, setClick] = useRecoilState(clickState);

  // Methods
  function onChoose() {
    setPet(title);
    setClick(click + 1);
  }

  return (
    <article className="card">
      <h2>{title}</h2>
      <img src={image} alt={title} />
      <button onClick={onChoose}>Choose me</button>
    </article>
  );
}
