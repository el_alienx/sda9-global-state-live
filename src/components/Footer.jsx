// NPM Package
import { useRecoilState } from "recoil";

// Project files
import { clickState } from "../state/siteAnalytics";

export default function Footer() {
  // Global State
  const [click, setClick] = useRecoilState(clickState);

  return (
    <footer className="footer">
      <hr />
      <small>Copyright 2021 - SDA</small>
      <p>User clicks: {click}</p>
    </footer>
  );
}
