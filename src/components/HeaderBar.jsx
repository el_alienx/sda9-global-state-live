// NPM Package
import { useRecoilState } from "recoil";

// Project files
import { petState } from "../state/userData";
import { clickState } from "../state/siteAnalytics";
import Logo from "../images/logo.svg";

export default function HeaderBar() {
  // Global state
  const [pet, setPet] = useRecoilState(petState);
  const [click, setClick] = useRecoilState(clickState);

  // Method
  function onClear() {
    setPet("No pet choosen");
    setClick(0);
  }

  return (
    <header className="header">
      <img src={Logo} className="logo" alt="Store logo" />
      <span className="pet-choosen">{pet}</span>
      <button onClick={onClear}>Clear choice</button>
    </header>
  );
}
