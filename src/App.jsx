// NPM Packages
import { RecoilRoot } from "recoil";

// Project files
import HeaderBar from "./components/HeaderBar";
import Content from "./components/Content";
import Footer from "./components/Footer";
import "./styles/style.css";

export default function App() {
  return (
    <div className="App">
      <RecoilRoot>
        <HeaderBar />
        <Content />
        <Footer />
      </RecoilRoot>
    </div>
  );
}
