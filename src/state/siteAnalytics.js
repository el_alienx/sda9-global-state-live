// NPM Packages
import { atom } from "recoil";

export const clickState = atom({
  key: "clickState",
  default: 0,
});
